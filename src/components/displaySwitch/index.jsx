import { useContext } from "react";
import { CartContext } from "../context/cartContext";

export default function DisplaySwitch() {
	const { display, toggleDisplay } = useContext(CartContext);

	const handleClick = () => {
		toggleDisplay();
		localStorage.setItem("CartDisplay", display);
	};

	return (
		<button className="switch" onClick={handleClick}>
			Change display
		</button>
	);
}
