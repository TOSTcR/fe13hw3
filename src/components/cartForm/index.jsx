import { useFormik } from "formik";
import { CartFormSchema } from "../schemas";
import { useSelector, useDispatch } from "react-redux";
import { alreadyInCart } from "../../redux/actions/alreadyInCart";
import { getItems } from "../../redux/actions/items";
import { itemsInCart } from "../../redux/actions/inCart";
import "./style.scss";

export default function CartForm(props) {
	const items = useSelector((state) => state.items);
	const dataInCart = useSelector((state) => state.alreadyInCart);
	const inCart = useSelector((state) => state.inCart);
	const dispatch = useDispatch();
	const filteredItems = dataInCart.map((el) =>
		items.find((item) => item.article === el)
	);
	const formik = useFormik({
		initialValues: {
			firstName: "",
			lastName: "",
			age: "",
			streetName: "",
			phoneNumber: "",
		},
		validationSchema: CartFormSchema,
		onSubmit: (values) => {
			console.log(values);
			checkout();
		},
	});

	function checkout() {
		filteredItems.map((el) => {
			console.log(
				`
				Item name ${el.name},
				Item price: ${el.price},
				Item picture: ${el.url},
				Item article: ${el.article},
				Item color: ${el.color},
				`
			);
		});
		dispatch(alreadyInCart([]));
		dispatch(itemsInCart(0));
		localStorage.setItem("ItemsInCart", 0);
		localStorage.setItem("CartsId", "");
	}
	return (
		<>
			<form onSubmit={formik.handleSubmit} className="cartForm">
				<h3>Order</h3>
				<label htmlFor="firstName">
					{formik.errors.firstName && formik.touched.firstName
						? formik.errors.firstName
						: "First Name"}
				</label>
				<input
					type="text"
					name="firstName"
					id="firstName"
					onChange={formik.handleChange}
					value={formik.values.firstName}
				/>
				<label htmlFor="lastName">
					{formik.errors.lastName && formik.touched.lastName
						? formik.errors.lastName
						: "Last Name"}
				</label>
				<input
					type="text"
					name="lastName"
					id="lastName"
					onChange={formik.handleChange}
					value={formik.values.lastName}
				/>
				<label htmlFor="age">
					{formik.errors.age && formik.touched.age ? formik.errors.age : "age"}
				</label>
				<input
					type="number"
					name="age"
					id="age"
					onChange={formik.handleChange}
					value={formik.values.age}
				/>
				<label htmlFor="streetName">
					{formik.errors.streetName && formik.touched.streetName
						? formik.errors.streetName
						: "street Name"}
				</label>
				<input
					type="text"
					name="streetName"
					id="streetName"
					onChange={formik.handleChange}
					value={formik.values.streetName}
				/>
				<label htmlFor="phoneNumber">
					{" "}
					{formik.errors.phoneNumber && formik.touched.phoneNumber
						? formik.errors.phoneNumber
						: "phone number"}
				</label>
				<input
					type="number"
					name="phoneNumber"
					id="phoneNumber"
					onChange={formik.handleChange}
					value={formik.values.phoneNumber}
				/>
				{filteredItems.length > 0 ? (
					<button type="submit">Checkout</button>
				) : (
					<button type="submit" disabled>
						Checkout
					</button>
				)}
			</form>
		</>
	);
}
