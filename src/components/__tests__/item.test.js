import { cleanup, fireEvent, screen } from "@testing-library/react";
import Item from "../item";
import { renderWithProviders } from "../../test-utils";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
let mockStore = configureStore();
let store;

beforeAll(() => {
	store = mockStore();
});

afterEach(cleanup);

test("element should render", () => {
	renderWithProviders(<Item />);
	expect(screen.getByTestId("item")).toBeInTheDocument();
});

test("should work on click", () => {
	const handleClick = jest.fn();
	renderWithProviders(<Item />);
	const button = screen.getByTestId("item-button");
	fireEvent.click(button);
	expect(handleClick).toHaveBeenCalled();
});

test("shapshot item", () => {
	const itemProps = {
		image: "https://dummyimage.com/600x400/59ffec/777ee0&text=lamp1",
		price: "1.00",
		name: "test",
		id: 1,
		color: "black",
		addToCart: jest.fn(),
		addToFav: jest.fn(),
		deleteFromFav: jest.fn(),
	};
	const tree = renderer
		.create(
			<Provider store={store}>
				<Item {...itemProps} />
			</Provider>
		)
		.toJSON();
	expect(tree).toMatchSnapshot();
});
