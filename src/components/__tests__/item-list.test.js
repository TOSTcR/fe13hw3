import { cleanup, screen } from "@testing-library/react";
import ItemList from "../itemList";
import { renderWithProviders } from "../../test-utils";
afterEach(cleanup);

test("element should render", () => {
	renderWithProviders(<ItemList />);
	expect(screen.getByTestId("item-list")).toBeInTheDocument();
});
