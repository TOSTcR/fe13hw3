import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import Modal from "../modal";
afterEach(cleanup);

test("should component exist", () => {
	render(<Modal />);
	expect(screen.getByTestId("modal")).toBeInTheDocument();
});

test("should work on click", () => {
	const handleClick = jest.fn();
	const mockData = {
		actions: (
			<button data-testid="yes" onClick={handleClick}>
				Yes
			</button>
		),
	};
	render(<Modal {...mockData} />);
	const button = screen.getByTestId("yes");
	fireEvent.click(button);
	expect(handleClick).toHaveBeenCalled();
});

test("should work on click", () => {
	const handleClick = jest.fn();
	const mockData = {
		actions: (
			<button data-testid="no" onClick={handleClick}>
				Yes
			</button>
		),
	};
	render(<Modal {...mockData} />);
	const button = screen.getByTestId("no");
	fireEvent.click(button);
	expect(handleClick).toHaveBeenCalled();
});
