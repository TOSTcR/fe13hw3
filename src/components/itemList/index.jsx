import { useSelector, useDispatch } from "react-redux";
import { getItems } from "../../redux/actions/items";
import { useEffect } from "react";
import Item from "../item";
import "./style.scss";

export default function ItemList(props) {
	const dispatch = useDispatch();
	const items = useSelector((state) => state.items);
	useEffect(() => {
		dispatch(getItems());
	}, [dispatch]);

	return (
		<div className="items" data-testid="item-list">
			{items.map((item) => {
				return (
					<Item
						image={item.url}
						price={item.price}
						name={item.name}
						id={item.article}
						key={item.article}
						color={item.color}
						addToCart={props.addToCart}
						addToFav={props.addToFavorite}
						deleteFromFav={props.deleteFromFavorite}></Item>
				);
			})}
		</div>
	);
}
