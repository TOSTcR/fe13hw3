import * as Yup from "yup";

export const CartFormSchema = Yup.object({
	firstName: Yup.string()
		.min(2, "Too short")
		.max(30, "Too long")
		.required("Field is required"),
	lastName: Yup.string()
		.min(2, "Too short")
		.max(30, "Too long")
		.required("Field is required"),
	age: Yup.number()
		.max(120, "You are already dead")
		.required("Field is required"),
	streetName: Yup.string()
		.max(120, "Too much letters")
		.required("Field is required"),
	phoneNumber: Yup.number().required("Field is required"),
});
