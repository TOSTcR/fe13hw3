import { createContext, useState } from "react";

const CartContext = createContext("standart");

function CartProvider(props) {
	const [display, setDisplay] = useState("standart");
	const toggleDisplay = () => {
		setDisplay(display === "standart" ? "table" : "standart");
	};
	return (
		<div>
			<CartContext.Provider value={{ display, toggleDisplay }}>
				{props.children}
			</CartContext.Provider>
		</div>
	);
}

export { CartContext, CartProvider };
