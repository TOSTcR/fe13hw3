import "./styles.scss";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

export default function Header(props) {
	const items = useSelector((state) => state.inCart);
	const favs = useSelector((state) => state.inFav);
	return (
		<nav className="nav" data-testid="header">
			<span className="nav-item nav__home">
				<NavLink to="/">Home</NavLink>
			</span>
			<span className="nav-item nav__cart">
				<NavLink to="/cart">Cart</NavLink>(Currently in cart {items ? items : 0}{" "}
				items)
			</span>
			<span className="nav-item nav__favorites">
				<NavLink to="/favorite">Favorites</NavLink>(Currently in favorites{" "}
				{favs ? favs : 0})
			</span>
		</nav>
	);
}

// Header.propTypes = {
// 	numOfFav: PropTypes.string.isRequired,
// };
