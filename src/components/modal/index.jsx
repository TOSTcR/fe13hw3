import "./style.scss";
import PropTypes from "prop-types";

export default function Modal(props) {
	return (
		<div
			className={props.modalClosed ? null : "overlay"}
			onClick={(e) => {
				if (e.target.className === "overlay") {
					props.close();
				}
			}}>
			<div
				className="modal"
				data-testid="modal"
				style={props.modalClosed ? { display: "none" } : { display: "flex" }}>
				<div className="modal__upperModal">
					<h3 className="modal__title">{props.header}</h3>
					{props.closeButton === true ? (
						<span
							className="modal__closeButton"
							onClick={() => {
								props.close();
							}}>
							X
						</span>
					) : null}
				</div>
				<p className="modal__text">{props.text}</p>
				{props.actions}
			</div>
		</div>
	);
}

// Modal.propTypes = {
// 	modalClosed: PropTypes.bool.isRequired,
// 	header: PropTypes.string.isRequired,
// 	closeButton: PropTypes.bool,
// 	close: PropTypes.func.isRequired,
// 	text: PropTypes.string.isRequired,
// 	actions: PropTypes.element.isRequired,
// };

// Modal.defaultProps = {
// 	closeButton: false,
// };
