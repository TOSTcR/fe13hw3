import Header from "../components/header";
import ItemList from "../components/itemList";

export default function Home(props) {
	return (
		<>
			<Header />
			<ItemList
				addToCart={props.addToCart}
				addToFavorite={props.addToFavorite}
				deleteFromFavorite={props.deleteFromFavorite}
			/>
		</>
	);
}
