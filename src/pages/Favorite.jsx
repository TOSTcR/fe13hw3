import { useSelector, useDispatch } from "react-redux";
import { getItems } from "../redux/actions/items";
import Header from "../components/header";
import FavoriteItem from "../components/item/favoriteItem";
import { useEffect } from "react";
export default function Favorite(props) {
	const dispatch = useDispatch();
	const items = useSelector((state) => state.items);
	const alreadyInFav = useSelector((state) => state.alreadyInFav);
	useEffect(() => {
		dispatch(getItems());
	}, [dispatch]);

	function renderItem() {
		let data = alreadyInFav.map((el) => {
			let id = items.find((item) => item.article === el);
			if (id === undefined) {
				id = items.find((item) => item.article === el);

				return id;
			}
			return id;
		});

		if (data[0] === undefined) {
			data = [
				{
					name: "lampt",
					price: 0,
					url: "https://dummyimage.com/600x400/59ffec/777ee0&text=lampt",
					article: "t",
					color: "orange",
				},
			];
		} else {
			data = alreadyInFav.sort().map((el) => {
				let id = items.find((item) => item.article === el);
				if (id === undefined) {
					id = items.find((item) => item.article === el);

					return id;
				}
				return id;
			});
		}

		const item = data.map((item) => {
			return (
				<FavoriteItem
					image={item.url}
					price={item.price}
					name={item.name}
					id={item.article}
					key={item.article}
					color={item.color}
					addToCart={props.addToCart}
					addToFav={props.addToFavorite}
					deleteFromFav={props.deleteFromFavorite}
				/>
			);
		});

		return item;
	}

	return (
		<>
			<Header />
			<div className="items">{renderItem()}</div>
		</>
	);
}
