import { useSelector, useDispatch } from "react-redux";
import { getItems } from "../redux/actions/items";
import Header from "../components/header";
import CartItem from "../components/item/cartItem";
import { useEffect, useContext, useReducer } from "react";
import CartForm from "../components/cartForm";
import { alreadyInCart } from "../redux/actions/alreadyInCart";
import { CartContext } from "../components/context/cartContext";
import CartItemTable from "../components/item/cartItemRow";
import DisplaySwitch from "../components/displaySwitch";
export default function Cart(props) {
	const dispatch = useDispatch();
	const items = useSelector((state) => state.items);
	const dataInCart = useSelector((state) => state.alreadyInCart);
	let { display } = useContext(CartContext);

	let localData;
	if (localStorage.getItem("CartsId") !== null) {
		localData = localStorage
			.getItem("CartsId")
			.split(",")
			.map((item) => parseInt(item));
	}

	useEffect(() => {
		dispatch(getItems());
		if (localData !== undefined) {
			if (!isNaN(localData[0])) {
				dispatch(alreadyInCart(localData));
			}
		}
	}, [dispatch]);

	function renderItem() {
		let data = dataInCart.map((el) => {
			let id = items.find((item) => item.article === el);
			if (id === undefined) {
				id = items.find((item) => item.article === el);

				return id;
			}
			return id;
		});

		if (data[0] === undefined) {
			if (localData === undefined) {
				data = [
					{
						name: "lampt",
						price: 0,
						url: "https://dummyimage.com/600x400/59ffec/777ee0&text=lampt",
						article: "t",
						color: "orange",
					},
				];
			} else {
				data = localData.sort().map((el) => {
					let id = items.find((item) => item.article === el);
					if (id === undefined) {
						id = items.find((item) => item.article === el);

						return id;
					}
					return id;
				});

				if (data[0] === undefined) {
					data = [
						{
							name: "lampt",
							price: 0,
							url: "https://dummyimage.com/600x400/59ffec/777ee0&text=lampt",
							article: "t",
							color: "orange",
						},
					];
				}
			}
		} else {
			data = dataInCart.sort().map((el) => {
				let id = items.find((item) => item.article === el);
				if (id === undefined) {
					id = items.find((item) => item.article === el);

					return id;
				}
				return id;
			});
		}

		const item = data.map((item) => {
			if (item.price !== 0) {
				if (display === "standart") {
					return (
						<CartItem
							image={item.url}
							price={item.price}
							name={item.name}
							id={item.article}
							key={item.article}
							color={item.color}
							deleteFromCart={props.deleteFromCart}
							addToFav={props.addToFavorite}
							deleteFromFav={props.deleteFromFavorite}
						/>
					);
				} else {
					return (
						<CartItemTable
							image={item.url}
							price={item.price}
							name={item.name}
							id={item.article}
							key={item.article}
							color={item.color}
							deleteFromCart={props.deleteFromCart}
							addToFav={props.addToFavorite}
							deleteFromFav={props.deleteFromFavorite}
						/>
					);
				}
			} else {
				return <h1>No items in cart</h1>;
			}
		});

		return item;
	}

	return (
		<>
			<Header />
			<DisplaySwitch />
			<div className="items">{renderItem()}</div>
			<CartForm deleteFromCart={props.deleteFromCart} />
		</>
	);
}
