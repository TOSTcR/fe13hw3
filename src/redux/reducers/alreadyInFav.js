import { alreadyInFavTypes } from "../types";

const initialState = [];

export function alreadyInFavReducer(state = initialState, action) {
	switch (action.type) {
		case alreadyInFavTypes.ALREADY_IN_FAV:
			return action.payload;
		default:
			return state;
	}
}
