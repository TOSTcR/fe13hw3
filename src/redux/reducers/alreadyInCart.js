import { alreadyInCartTypes } from "../types";

const initialState = [];

export function alreadyInCartReducer(state = initialState, action) {
	switch (action.type) {
		case alreadyInCartTypes.ALREADY_IN_CART:
			return action.payload;
		default:
			return state;
	}
}
