import { inCartTypes } from "../types";

const initialState = 0;

export function inCartReducer(state = initialState, action) {
	switch (action.type) {
		case inCartTypes.IN_CART:
			return action.payload;
		default:
			return state;
	}
}
