import { modalTypes } from "../types";

const initialState = true;

export function modalReducer(state = initialState, action) {
	switch (action.type) {
		case modalTypes.IS_CLOSED:
			return action.payload;
		default:
			return state;
	}
}
