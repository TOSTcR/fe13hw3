import { itemTypes } from "../types";

const initialState = [];

export function itemsReducer(state = initialState, action) {
	switch (action.type) {
		case itemTypes.FILL_ITEMS:
			return action.payload;
		default:
			return state;
	}
}
