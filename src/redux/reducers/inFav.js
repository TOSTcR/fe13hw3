import { inFavTypes } from "../types";

const initialState = 0;

export function inFavReducer(state = initialState, action) {
	switch (action.type) {
		case inFavTypes.IN_FAV:
			return action.payload;
		default:
			return state;
	}
}
