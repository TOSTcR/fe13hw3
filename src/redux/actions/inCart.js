import { inCartTypes } from "../types";

export function itemsInCart(amount) {
	return {
		type: inCartTypes.IN_CART,
		payload: amount,
	};
}
