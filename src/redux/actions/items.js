import { itemTypes } from "../types";

export function fillItems(data) {
	return {
		type: itemTypes.FILL_ITEMS,
		payload: data,
	};
}

export function getItems() {
	return async function (dispatch) {
		const data = await fetch("./index.json").then((res) => res.json());
		dispatch(fillItems(data));
	};
}
