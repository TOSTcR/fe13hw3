import { alreadyInFavTypes } from "../types";

export function alreadyInFavorite(data) {
	return {
		type: alreadyInFavTypes.ALREADY_IN_FAV,
		payload: data,
	};
}
