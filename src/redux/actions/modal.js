import { modalTypes } from "../types";

export function isModalClosed(isClosed) {
	return {
		type: modalTypes.IS_CLOSED,
		payload: isClosed,
	};
}
