import { inFavTypes } from "../types";

export function itemsInFavorite(amount) {
	return {
		type: inFavTypes.IN_FAV,
		payload: amount,
	};
}
