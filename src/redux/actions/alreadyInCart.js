import { alreadyInCartTypes } from "../types";

export function alreadyInCart(data) {
	return {
		type: alreadyInCartTypes.ALREADY_IN_CART,
		payload: data,
	};
}
