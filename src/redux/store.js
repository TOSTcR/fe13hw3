import { createStore, applyMiddleware } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "./rootReducer";
import { composeEnhancers, middleware } from "./middlewares";

export const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(...middleware))
);

export const setupStore = (preloadedState) => {
	return configureStore({
		reducer: rootReducer,
		preloadedState,
	});
};
