import { combineReducers } from "redux";

import { itemsReducer as items } from "./reducers/items";
import { modalReducer as modal } from "./reducers/modal";
import { inCartReducer as inCart } from "./reducers/inCart";
import { inFavReducer as inFav } from "./reducers/inFav";
import { alreadyInFavReducer as alreadyInFav } from "./reducers/alreadyInFav";
import { alreadyInCartReducer as alreadyInCart } from "./reducers/alreadyInCart";

export const rootReducer = combineReducers({
	items,
	modal,
	inCart,
	inFav,
	alreadyInFav,
	alreadyInCart,
});
