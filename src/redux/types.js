export const itemTypes = {
	FILL_ITEMS: "FILL_ITEMS",
};

export const modalTypes = {
	IS_CLOSED: "IS_CLOSED",
};

export const inCartTypes = {
	IN_CART: "IN_CART",
};

export const inFavTypes = {
	IN_FAV: "IN_FAV",
};

export const alreadyInFavTypes = {
	ALREADY_IN_FAV: "ALREADY_IN_FAV",
};

export const alreadyInCartTypes = {
	ALREADY_IN_CART: "ALREADY_IN_CART",
};
