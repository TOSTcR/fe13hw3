import { alreadyInCartReducer } from "../reducers/alreadyInCart";
import { alreadyInCart } from "../actions/alreadyInCart";

test("should change state", () => {
	const initialState = [];
	const newState = [2];
	expect(alreadyInCartReducer(initialState, alreadyInCart(newState))).toEqual(
		newState
	);
});
