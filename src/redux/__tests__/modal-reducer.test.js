import { modalReducer } from "../reducers/modal";
import { isModalClosed } from "../actions/modal";

test("should change state ", () => {
	const initialState = true;
	const newState = false;
	expect(
		modalReducer(
			initialState,
			isModalClosed(initialState ? newState : initialState)
		)
	).toEqual(newState);
});
