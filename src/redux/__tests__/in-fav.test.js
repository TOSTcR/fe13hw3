import { inFavReducer } from "../reducers/inFav";
import { itemsInFavorite } from "../actions/inFav";

test("should change state", () => {
	const initialState = 0;
	const newState = 1;
	expect(inFavReducer(initialState, itemsInFavorite(newState))).toEqual(
		newState
	);
});
