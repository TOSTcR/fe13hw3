import { inCartReducer } from "../reducers/inCart";
import { itemsInCart } from "../actions/inCart";

test("should change state", () => {
	const initialState = 0;
	const newState = 1;
	expect(inCartReducer(initialState, itemsInCart(newState))).toEqual(newState);
});
