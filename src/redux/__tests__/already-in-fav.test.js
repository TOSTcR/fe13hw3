import { alreadyInFavReducer } from "../reducers/alreadyInFav";
import { alreadyInFavorite } from "../actions/alreadyInFav";

test("should change state", () => {
	const initialState = [];
	const newState = [2];
	expect(
		alreadyInFavReducer(initialState, alreadyInFavorite(newState))
	).toEqual(newState);
});
