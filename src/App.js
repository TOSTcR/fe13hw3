import { Route, Routes } from "react-router-dom";
import { CartProvider } from "./components/context/cartContext";
import { useDispatch, useSelector } from "react-redux";
import { itemsInCart } from "./redux/actions/inCart";
import { alreadyInFavorite } from "./redux/actions/alreadyInFav";
import { itemsInFavorite } from "./redux/actions/inFav";
import { alreadyInCart } from "./redux/actions/alreadyInCart";
import { useEffect } from "react";
import Favorite from "./pages/Favorite";
import Cart from "./pages/Cart";
import Home from "./pages/Home";

function App() {
	const dispatch = useDispatch();
	const inCart = useSelector((state) => state.inCart);
	const alreadyInFav = useSelector((state) => state.alreadyInFav);
	const inFav = useSelector((state) => state.inFav);
	const dataInCart = useSelector((state) => state.alreadyInCart);
	let localFavs;

	useEffect(() => {
		onLoad();
	});

	function onLoad() {
		findFavoriteOnLoad();
		if (localStorage.getItem("NumOfFavs")) {
			dispatch(itemsInFavorite(parseInt(localStorage.getItem("NumOfFavs"))));
		} else {
			dispatch(itemsInFavorite(inFav));
			localStorage.setItem("NumOfFavs", inFav);
		}

		if (localStorage.getItem("ItemsInCart")) {
			dispatch(itemsInCart(parseInt(localStorage.getItem("ItemsInCart"))));
		} else {
			dispatch(itemsInCart(inCart));
			localStorage.setItem("ItemsInCart", inCart);
		}

		// if (alreadyInFav.length === 0) {
		// 	dispatch(itemsInFavorite(0));
		// }
	}

	function findFavoriteOnLoad() {
		if (alreadyInFav.length === 0 && localStorage.getItem("idsInFav")) {
			localFavs = localStorage
				.getItem("idsInFav")
				.split(",")
				.map((item) => parseInt(item));
			dispatch(alreadyInFavorite(localFavs));
		}
	}

	function addToCart(id) {
		if (inCart.length < 0) {
			dispatch(alreadyInCart([+id]));
			dispatch(itemsInCart(inCart + 1));
			localStorage.setItem("ItemsInCart", inCart + 1);
			localStorage.setItem("CartsId", [+id]);
		} else {
			if (dataInCart.some((item) => item === id)) {
				const index = dataInCart.map((e) => e).findIndex((e) => e === id);
				dispatch(alreadyInCart([...dataInCart.splice(index, 1)]));
				localStorage.setItem("CartsId", [...dataInCart.splice(index, 1)]);
			}

			dispatch(alreadyInCart([...dataInCart, +id]));
			dispatch(itemsInCart(inCart + 1));
			localStorage.setItem("ItemsInCart", inCart + 1);
			localStorage.setItem("CartsId", [...dataInCart, +id]);
		}
	}

	function deleteFromCart(id) {
		dispatch(alreadyInCart([...dataInCart.filter((item) => item !== id)]));
		dispatch(itemsInCart(inCart - 1));
		localStorage.setItem("ItemsInCart", inCart - 1);
		localStorage.setItem("CartsId", [
			...dataInCart.filter((item) => item !== id),
		]);
	}

	function addToFavorite(id) {
		if (inFav.length < 0) {
			dispatch(alreadyInFavorite([+id]));
			dispatch(itemsInFavorite(inFav + 1));
			localStorage.setItem("NumOfFavs", inFav + 1);
			localStorage.setItem("idsInFav", [+id]);
		} else {
			if (alreadyInFav.some((item) => item === id)) {
				const index = alreadyInFav.map((e) => e).findIndex((e) => e === id);
				dispatch(alreadyInFavorite([...alreadyInFav.splice(index, 1)]));
				dispatch(itemsInFavorite(inFav + 1));
				localStorage.setItem("NumOfFavs", inFav + 1);
				localStorage.setItem("idsInFav", [...alreadyInFav.splice(index, 1)]);
			}
			dispatch(alreadyInFavorite([...alreadyInFav, +id]));
			dispatch(itemsInFavorite(inFav + 1));
			localStorage.setItem("NumOfFavs", inFav + 1);
			localStorage.setItem("idsInFav", [...alreadyInFav, +id]);
		}
	}

	function deleteFromFavorite(id) {
		dispatch(
			alreadyInFavorite([...alreadyInFav.filter((item) => item !== id)])
		);

		dispatch(itemsInFavorite(inFav - 1));
		localStorage.setItem("NumOfFavs", inFav - 1);
		localStorage.setItem("idsInFav", [
			...alreadyInFav.filter((item) => item !== id),
		]);
	}

	return (
		<CartProvider>
			<Routes>
				<Route
					path="/"
					element={
						<Home
							addToCart={addToCart}
							addToFavorite={addToFavorite}
							deleteFromFavorite={deleteFromFavorite}
						/>
					}></Route>
				<Route
					path="favorite"
					element={
						<Favorite
							addToCart={addToCart}
							addToFavorite={addToFavorite}
							deleteFromFavorite={deleteFromFavorite}
						/>
					}></Route>
				<Route
					path="cart"
					element={
						<Cart
							deleteFromCart={deleteFromCart}
							addToFavorite={addToFavorite}
							deleteFromFavorite={deleteFromFavorite}
						/>
					}></Route>
			</Routes>
		</CartProvider>
	);
}

export default App;
